<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 10: CS 358, Fall 2022</title>
  </head>

  <body>
    <h1>CS 358, Fall 2022</h1>
    <h2>Lecture 10: Survey of object-oriented programming</h2>

    <h3>Introduction</h3>

      <p>Last lecture, we discussed the concept of <i>programming paradigms</i>.</p>

      <p>Today, we'll focus on the paradigm of <i>object-oriented programming (OOP)</i>.</p>

      <p>What features are associated with the OOP paradigm?</p>

      <p>How are these features implemented in compilers and interpreters?</p>

      <p>How does OOP influence the way that we think about code?</p>

      <p>We'll <b>survey</b> these questions at a relatively shallow level.</p>

      <p>One week is not enough time to really learn OOP!</p>

      <p>Honestly, ten weeks is not enough time, but it's enough to get started.</p>

      <p>At PSU, you can study OOP more deeply in courses about <i>software design patterns</i> and the Java language.</p>

    <h3>Definitions of OOP</h3>

      <p>In previous lectures, I've argued that OOP is <b>defined</b> by the feature of <i>dynamic dispatch</i>.</p>

      <p>I do think this is an accurate perspective, but it's a very theoretical perspective.</p>

      <p>In practice, there are a variety of different conceptions of what the "essence" of OOP is.</p>

      <p>In this lecture, we will explore a couple historically important perspectives and their influence on OOP language design.</p>

    <h3>Pre-history of OOP</h3>

      <p>In the early 1960s, Ole-Johan Dahl and Kristen Nygaard invented the Simula language at the Norwegian Computing Center.</p>

      <p>Simula is a direct descendant of ALGOL 60, one of the earliest <i>procedural</i> languages.</p>

      <p>Simula 67 (from 1967) is now commonly considered to be the first OOP language.</p>

      <p>It remained relatively obscure in software development, but influenced many later OOP language designs.</p>

      <h4>Garbage collection</h4>

        <p>Simula 67 supports the feature of <i>garbage collection</i>.</p>

        <p>In a garbage-collected OOP language, objects are <i>deallocated</i> automatically sometime after the program no longer needs them.</p>

        <p>This is guaranteed to be safe: the garbage collector never deallocates memory that might still be in use.</p>

        <p>The programmer never explicitly deallocates memory at all.</p>

        <p>This is a tradeoff:</p>

        <ul>
          <li>We gain <i>safety</i>, because there's no possibility of programmer error in deallocating memory.</li>
          <li>We gain convenience, because the programmer doesn't have to think about when to deallocate memory.</li>
          <li>We lose a little performance, because the garbage collector has to do work at runtime in order to discover when objects are no longer needed.</li>
        </ul>

        <p>Rust's borrow checker achieves the same safety without the performance loss, but does not achieve quite the same convenience.</p>

        <p>Garbage collection is not necessary for OOP, but it is historically associated with OOP.</p>

        <p>Many common OOP patterns are highly error-prone when deallocating objects manually.</p>

    <h3>Smalltalk-style OOP</h3>

      <h4>History</h4>

        <p>In 1972, Alan Kay invented the Smalltalk language, which is commonly credited with popularizing OOP.</p>

        <p>Smalltalk implements a form of <i>dynamically-typed</i> OOP.</p>

        <p>Smalltalk is usually <i>interpreted</i>, but there have been compilers for some dialects.</p>

        <p>Apple's Objective-C is a direct descendant of Smalltalk.</p>

        <p>Smalltalk's influence is most visible today in Python and JavaScript.</p>

        <p>The <i>Web platform</i> is also based directly on the design of Smalltalk's interpreter.</p>

      <h4>Concept</h4>

        <p>Alan Kay's concept of OOP was originally influenced by his study of microbiology.</p>

        <p>The analogy goes like this:</p>

        <ul>
          <li>An <i>object</i> is like an individual micro-organism.</li>
          <li>Each object contains its own <i>state</i>, like how each micro-organism has its own chemical contents.</li>
          <li>Objects communicate by sending <i>messages</i>, like how micro-organisms send chemical signals to each other.</li>
        </ul>

        <p>In Smalltalk, the term <i>message</i> is analogous to the modern term <i>method call</i>.</p>

        <p>Kay was mostly concerned with designing an <i>expressive</i> language, suitable for general-purpose use including teaching and application development.</p>

        <p>In Kay's article "The Early History of Smalltalk", he defines OOP in six core principles, quoted here (emphasis his):</p>

        <ol>
          <li>Everything is an object.</li>
          <li>Objects communicate by sending and receiving <b>messages</b> (in terms of objects).</li>
          <li>Objects have their <b>own memory</b> (in terms of objects).</li>
          <li>Every object is an instance of a <b>class</b> (which must be an object).</li>
          <li>The class holds the shared <b>behavior</b> for its instances (in the form of objects in a program list).</li>
          <li>To eval a program list, control is passed to the first object and the remainder is treated as its message.</li>
        </ol>

        <p>Number 6 is a little weird, but most of these are still recognizable in OOP today.</p>

        <p>Numbers 2 and 3 are describing the modern concept of <i>encapsulation</i>.</p>

        <p>This is not a universally-accepted definition of OOP, though!</p>

        <p>Here's my attempt at a more modern phrasing:</p>

        <ol>
          <li>Every <i>value</i> that exists at runtime is represented with the same basic "object" data structure. (Primitive values like <span class="code">1</span> and <span class="code">true</span> are <i>boxed</i> in "wrapper" objects.)</li>
          <li>Objects interact by calling <i>methods</i> on each other.</li>
          <li>Each object has its own <i>state</i>, separate from any other object's state.</li>
          <li>Each object is created using a <i>class object</i> as a template. Each object's state stores a reference to the class object that was used to create it.</li>
          <li>Each class object's state contains the code for each method that the class defines. When a method is called on an object, the object looks in its class object to find the code to execute.</li>
          <li>Every program starts by calling a "<span class="code">main</span> function" on some "first" object.</li>
        </ol>

        <p>(What about "and the remainder is treated as its message"? In my reading, this most closely corresponds to the concept of a monad in FP, which we won't have time to cover in this course.)</p>

        <p>Python and JavaScript implement all of these principles (roughly).</p>

        <p>C++/C#/Java violate principle 1: they have <i>values</i> that are not <i>objects</i>.</p>

        <p>C++ also violates principle 4 and 5: its classes do not exist at runtime.</p>

        <p>C++ also violates principle 6: its <span class="code">main</span> function is not a method on an object.</p>

      <h4>Dynamic dispatch</h4>

        <p>Principle 5 defines the feature of <i>dynamic dispatch</i> in Smalltalk.</p>

        <p>In Smalltalk-style OOP, <b>classes exist at runtime</b> as class objects.</p>

        <p>In modern terms, this is an implementation based on <i>prototypes</i>.</p>

        <p>The term <i>class object</i> corresponds to a <i>prototype</i> in JavaScript terminology.</p>

        <p>At <b>runtime</b>:</p>

        <ol>
          <li>A message <span class="code">f(x)</span> is sent to an object <span class="code">obj</span>: in Smalltalk this is written <span class="code">obj f: x</span>.</li>
          <li><span class="code">obj</span> accesses its class object, <span class="code">c</span>.</li>
          <li>
            <ul>
              <li>If <span class="code">c</span> has a definition of <span class="code">f</span>:
                <ul>
                  <li>That definition is called, with <span class="code">x</span> as the argument and <span class="code">obj</span> as the <span class="code">this</span> (or <span class="code">self</span>) object.</li>
                </ul>
              </li>
              <li>If <span class="code">c</span> doesn't have a definition of <span class="code">f</span>:
                <ul>
                  <li>If <span class="code">c</span> has a reference to a superclass object:
                    <ul>
                      <li>Try step 3 again, with the superclass object instead of <span class="code">c</span>.</li>
                    </ul>
                  </li>
                  <li>If <span class="code">c</span> doesn't have a reference to a superclass object:
                    <ul>
                      <li>Throw a runtime "method not found" exception.</li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ol>

    <h3>C++-style OOP</h3>

      <p>Why is C++ so different from Smalltalk's style of OOP?</p>

      <h4>History</h4>

        <p>In the early 1980s, Bjarne Stroustrup invented C++, a direct descendant of both C and Simula 67.</p>

        <p>C++ is one of very few <i>low-level</i> OOP languages in history.</p>

        <p>C++ implements a form of <i>statically-typed</i> OOP, although its type system was relatively minimal at first.</p>

        <p>C++ is usually <i>compiled</i>, although interpreters for it have been developed.</p>

        <p>C++ aims to maixmize performance at all costs, which sets it apart from most other OOP languages.</p>

        <p>C++ has no garbage collector, trading safety and convenience for performance.</p>

      <h4>Concept</h4>

        <p>Stroustrup was mostly concerned with the problem of large C codebases becoming unmaintainable.</p>

        <p>In Stroustrup's article "What is 'Object-Oriented Programming'?", he gives a definition of the OOP paradigm, quoted here (formatting his):</p>

        <blockquote class="centered">
          Decide which classes you want;<br>
          provide a full set of operations for each class;<br>
          make commonality explicit by using inheritance.
        </blockquote>

        <p>This is much less focused on <i>objects</i> than Alan Kay's definition is.</p>

        <p>Stroustrup's OOP is focused on the feature of <i>inheritance</i>, as a way to achieve <i>code reuse</i>.</p>

        <p>To Stroustrup, <i>encapsulation</i> helps programmers communicate the boundaries of their code to each other.</p>

        <p>The need for <i>dynamic dispatch</i> is motiviated by the goal of better encapsulation.</p>

        <p>C++ popularized the <span class="code">public</span> and <span class="code">private</span> modifiers, which it inherited from Simula 67.</p>

        <p>In C++, a <span class="code">class</span> is <b>just</b> a <span class="code">struct</span> whose fields are <span class="code">private</span> by default.</p>

        <p>This means a <span class="code">struct</span> with <span class="code">virtual</span> methods is just as "object-oriented" as a <span class="code">class</span> with <span class="code">virtual</span> methods!</p>

      <h4>Dynamic dispatch</h4>

        <p>The feature of <i>virtual methods</i> implements <i>dynamic dispatch</i> in C++.</p>

        <p>In C++-style OOP, <b>classes only exist at compile time</b>, not at runtime.</p>

        <p>C++ uses is an implementation of dynamic dispatch based on <i>virtual tables</i>.</p>

        <p>At <b>compile-time</b>:</p>

        <ul>
          <li>For each class in the program:
            <ol>
              <li>Generate a static array of pointers, one for each <span class="code">virtual</span> method that the class defines or overrides. This is the <i>virtual table</i>, or <i>vtable</i>.</li>
              <li>When compiling the constructor, include additional code to store a pointer to the class's vtable in each constructed object.</li>
              <li>Compile each non-<span class="code">virtual</span> method to a function, with an extra argument for the <span class="code">this</span> pointer.</li>
              <li>For each method that overrides a <span class="code">virtual</span> method, after compiling the method, store its address in the vtable.</li>
              <li>Pure <span class="code">virtual</span> methods don't get compiled at all: they're left as empty slots in the vtable, which subclass constructors will fill in.</li>
            </ol>
          </li>
          <li>For each method call on an object, <span class="code">obj-&gt;f(x)</span>:
            <ul>
              <li>If <span class="code">f</span> is non-<span class="code">virtual</span>, compile the call as a jump to a known constant address.</li>
              <li>If <span class="code">f</span> is <span class="code">virtual</span>, compile the call as a jump to an address stored in the class's vtable, which is accessed by the pointer stored in <span class="code">obj</span>.</li>
            </ul>
          </li>
        </ul>

        <p>Ultimately, this works pretty similarly to Smalltalk's implementation in most use cases.</p>

        <p>The main difference is that in C++, <b>classes don't exist at runtime</b>.</p>

        <p>A vtable is just a static array of pointers.</p>

        <p>In particular, there is no language feature for creating a new vtable at runtime.</p>

        <p>You can hack a vtable together with raw pointer manipulation, but that's not a deliberate use case.</p>

        <p>In Smalltalk-style OOP, the language usually provides a deliberate feature for creating new classes at runtime.</p>

    <h3>Eiffel-style OOP</h3>

      <p>Okay, so surveying the modern landscape again:</p>

      <p>Python and JavaScript inherit from Smalltalk.</p>

      <p>Hardly any languages follow C++ in implementing <i>low-level</i> OOP.</p>

      <p>What about languages like Java and C#?</p>

      <p>In some ways, they combine features of Smalltalk-style OOP and C++-style OOP.</p>

      <h4>History</h4>

        <p>In 1986, Eiffel Software released the Eiffel language, invented by Bertrand Meyer.</p>

        <p>Eiffel implements a form of OOP similar to Smalltalk's, but with <i>static typechecking</i>.</p>

        <p>One exception is that classes are not objects in Eiffel (but they are in many descendants of Eiffel).</p>

        <p>Eiffel is usually <i>compiled</i>, and uses some dynamic dispatch optimization tricks taken from C++.</p>

      <h4>Concept</h4>

        <p>Eiffel's biggest historical contribution to OOP is the concept of <i>design by contract</i>.</p>

        <p>In Bertrand Meyer's book "Object Oriented Software Construction", he gives a definition of design by contract, quoted in part here:</p>

        <blockquote>
          [...] viewing the relationship between a class and its clients as a formal agreement, expressing each party's rights and obligations.
        </blockquote>

        <p>Some fundamental concepts in design by contract are:</p>

        <ul>
          <li><i>Preconditions</i>, which express what a method <b>requires</b> to be true <b>before</b> it runs.</li>
          <li><i>Postconditions</i>, which express what a method <b>causes</b> to be true <b>after</b> it runs.</li>
          <li><i>Invariants</i>, which express what a method <b>does not affect</b> when it runs.</li>
        </ul>

        <p>Eiffel has features to express each of these in the <i>definition</i> of a method.</p>

        <p>These conditions compile to runtime checks that execute before and/or after the method runs.</p>

        <p>This example is taken from the Eiffel <a href="https://www.eiffel.org/doc/solutions/Design_by_Contract_and_Assertions">Design by Contract and Assertions</a> article:</p>

        <pre class="code">
set_second (s: INTEGER)
        -- Set the second from `s'.
    require
        valid_argument_for_second: 0 &lt;= s and s &lt;= 59
    do
        second := s
    ensure
        second_set: second = s
    end</pre>

        <p>This specific feature is not common in newer languages, but it is an early influential example of the <i>test-driven development</i> paradigm.</p>

        <p>The design of the Java language is heavily influenced by Eiffel and the design by contract philosophy.</p>

        <p>Java exploded in popularity in the 90s and 2000s, so we see Eiffel's influence by way of Java in many modern languages.</p>

        <p>(We're more likely to call this style "Java-style OOP" in the modern world, but Eiffel is usually identified as the historical root of it.)</p>

        <h4>Dynamic dispatch</h4>

          <p>Java's <span class="code">interface</span> feature is its central <i>design by contract</i> feature.</p>

          <p>In terms of implementation, an <i>interface</i> is just an <i>abstract base class</i>.</p>

          <p>The implementation is very similar to virtual methods in C++.</p>

          <p>An <span class="code">interface</span> serves a special purpose in design by contract, though!</p>

          <p>Specifically, <b>an <span class="code">interface</span> represents a contract</b>.</p>

          <p>Part of the contract is <i>static</i>: a <span class="code">class</span> that <span class="code">implement</span>s an <span class="code">interface</span> is guaranteed to provide methods with the correct names and types.</p>

          <p>Part of the contract is <i>dynamic</i>: an <span class="code">interface</span> often comes with <i>tests</i> checking the preconditions, postconditions, and invariants of each method.</p>

          <p>The Java language does not attach the tests to the <span class="code">interface</span> syntactically, but they are conceptually "part of" the <span class="code">interface</span>.</p>

          <p>By this convention, Java software design is oriented around <span class="code">interface</span>s as units of <i>encapsulation</i>.</p>

          <p>The feature of <i>dynamic dispatch</i> is what makes this design approach possible.</p>

    <h3>Subclassing</h3>

      <p>A core feature of <i>subclassing</i> is the <i>Liskov substitution principle</i>.</p>

      <p>This is present in every OOP language, and many non-OOP languages too.</p>

      <p>The general principle is:</p>

      <ul>
        <li>When a method <b>takes in</b> an argument, the argument's type may be <b>any subclass</b> of the type that the method expects.</li>
        <li>When a method <b>returns</b> a return value, the value's type may be <b>any subclass</b> of the type that the caller expects.</li>
      </ul>

      <p>In dynamically-typed languages, this is a natural result of the dynamic type system, and is sometimes referred to as <i>duck typing</i>.</p>

      <p>In statically-typed languages, this is achieved by making the typechecker aware of subclass hierarchies.</p>

      <p>Most class-based object-oriented languages use a keyword like <span class="code">extends</span> or a symbol like <span class="code">:</span> to indicate subclassing.</p>

    <h3>Multiple inheritance</h3>

      <p>Is multiple inheritance bad?</p>

      <p>Let's survey a couple perspectives.</p>

      <h4>Uses of multiple inheritance</h4>

        <p>Imagine I'm building a driving video game.</p>

        <p>I have a <span class="code">Vehicle</span> class for every drivable thing in my game.</p>

        <p>I also have a <span class="code">Surface</span> class for everything that can be painted different colors.</p>

        <p>Not all <span class="code">Vehicle</span>s are <span class="code">Surface</span>s, because some of them are special antimatter vehicles or something.</p>

        <p>Not all <span class="code">Surface</span>s are <span class="code">Vehicle</span>s either, because the drivers' clothes are also <span class="code">Surface</span>s.</p>

        <p>So <span class="code">Vehicle</span> shouldn't be a subclass or superclass of <span class="code">Surface</span>.</p>

        <p>But if we want a class for a car that can be painted different colors, we want a <span class="code">Surface</span> that's also a <span class="code">Vehicle</span>, right?</p>

        <p>This is a use case of multiple inheritance.</p>

        <p>Our car class can inherit from both <span class="code">Surface</span> and <span class="code">Vehicle</span>, and will work in contexts expecting either class.</p>

      <h4>Downsides of multiple inheritance</h4>

        <p>Imagine I'm building a golf game within my driving video game.</p>

        <p>There are <span class="code">Vehicle</span>s, which can <span class="code">drive</span> from point A to point B.</p>

        <p>There are also <span class="code">Golfer</span>s, which can <span class="code">drive</span> a golf ball in a different sense.</p>

        <p>Now, in a special promotion, we have a <span class="code">Vehicle</span> that is also a <span class="code">Golfer</span>.</p>

        <p>When we call its <span class="code">.drive()</span> method, what does it do?</p>

        <p>It can try to "guess" which is the right meaning, but it can't guess perfectly.</p>

        <p>At best, we're going to get a scope-checking error saying that it's ambiguous.</p>

        <p>This is sometimes known as the <i>diamond problem</i>.</p>

      <h4>Multiple inheritance vs. dynamic dispatch</h4>

        <p>In most real-world languages, we could specify which we want by writing something like <span class="code">((Vehicle) obj).drive()</span> or <span class="code">obj.Golfer::drive()</span>.</p>

        <p>But this is <i>static dispatch</i>, not <i>dynamic dispatch</i>!</p>

        <p>The one feature of OOP that everyone agrees upon is that <b>objects choose how to handle their own method calls</b>.</p>

        <p>If the programmer is making that choice explicitly at every call site, we're hardly doing OOP.</p>

        <p>This is the major downside of multiple inheritance: it can ruin dynamic dispatch.</p>

        <p>In practice, this is a tradeoff, but rarely considered worth the cost.</p>

        <p>Some OOP languages have no support for multiple inheritance at all, notably Java and JavaScript.</p>

      <h4>Single inheritance, multiple implementation</h4>

        <p>Java introduced a middle ground that has become popular:</p>

        <ul>
          <li><i>Single inheritance</i>: a class may only <i>inherit</i> method <i>definitions</i> from one other <i>class</i>.</li>
          <li><i>Multiple implementation</i>: a class may <i>implement</i> method <i>overrides</i> for any number of <i>interfaces</i>.</li>
        </ul>

        <p>This allows our <span class="code">Vehicle</span> to also be a <span class="code">Surface</span>, as long as one or both of those classes is an interface.</p>

        <p>In practice, it's likely that <span class="code">Surface</span> can be an interface, so this works out.</p>

        <p>Our <span class="code">Vehicle</span> <span class="code">Golfer</span> still has a problem, but at least it's a simpler problem.</p>

        <p>Instead of discovering that it <i>inherited</i> two completely different <span class="code">drive</span> methods, we discover the ambiguity when we try to <i>implement</i> a single unique <span class="code">drive</span> method definition in the car subclass.</p>

        <p>C# and TypeScript both implement this same feature set of <i>single inheritance, multiple implementation</i>.</p>

      <h4>Alternatives</h4>

        <p>There have been many language features proposed and implemented to address the diamond problem.</p>

        <p>In general, the goal is to achieve some of the convenience of multiple inheritance without the drawbacks.</p>

        <p>These include features named <i>mixins</i> and <i>traits</i>, which show up in a variety of later languages inspired by Java and C#.</p>

        <p>Both Java and C# have also recently added support for more complex forms of "interfaces".</p>

        <p>Rust's "traits" are slightly different from OOP traits, but have a pretty similar use case.</p>

        <p>Rust is not an OOP language, though: it does not have <i>objects</i> in the traditional sense.</p>

        <p>Rust is arguably an example of a new paradigm of "trait-oriented" procecural languages with strong FP influence.</p>

        <p>Language design is not done yet!</p>

    <h3>Looking forward</h3>

      <p>In the next lecture and assignment, we will explore OOP in practice with TypeScript.</p>

      <p>We'll clean up our old interpreter code with some OOP design techniques.</p>

      <p>After that, we'll turn our attention to a brief survey of <i>functional programming</i>.</p>

      <p>We'll clean up our intepreter code in different ways with some FP design techniques, and discuss its relation to OOP.</p>

      <p>Finally, we'll finish up the course with a comparative discussion of real-world languages, and how to think about programming languages in your practice as a software developer.</p>

      <p>There's not much time left in the quarter - hang in there!</p>
  </body>

</html>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 2: CS 358, Fall 2022</title>
  </head>

  <body>
    <h1>CS 358, Fall 2022</h1>
    <h2>Lecture 2: Computing platforms</h2>

    <h3>Introduction</h3>

      <p>The ultimate goal of these lecture slides is to explain how TypeScript code runs.</p>

      <p>This will take us some work to build up to!</p>

      <p>On the way, we'll touch on <i>compilers</i> and <i>interpreters</i>.</p>

    <h3>Why TypeScript?</h3>

      <p>Before we get going: why are we using TypeScript in this course?</p>

      <ul>
        <li>It runs on any computer.</li>
        <li>It only requires minimal installation work.</li>
        <li>It's a good example of a recently-created language.</li>
        <li>It allows us to explore several different PL paradigms.</li>
      </ul>

      <h4>Disclaimer</h4>

        <p>The goal of this course is not to teach you TypeScript.</p>

        <p>We will use TypeScript in some non-standard ways.</p>

        <p>You will not come out of this with new web development skills.</p>

        <p><b>TypeScript is the tool we will use to investigate PL concepts.</b></p>

        <p>"Computer science is no more about computers than astronomy is about telescopes." -Edsger Dijkstra</p>

    <h3>How does code run?</h3>

      <p>A <i>computing platform</i> is an environment that can run code.</p>

      <p>This is a pretty broad term that includes hardware and software.</p>

      <p>Platforms <i>stack</i>: one platform may run "on top of" another.</p>

      <p>(You might have heard of <i>virtualization</i> or <i>emulation</i>.)</p>

    <h3>Physical machines</h3>

      <p>Our computers only really understand one language: machine code.</p>

      <p>Different processor architectures have different machine codes.</p>

      <p>A processor architecture is a platform for a machine code.</p>

      <p>This is the "ground level" of our platform stack (as software people).</p>

      <p>How do we run other, non-machine-code languages?</p>

    <h3>Compilers</h3>

      <p>A <i>compiler</i> translates code from one language to another.</p>

      <p>The input language is the <i>source</i> language.</p>

      <p>The output language is the <i>target</i> language.</p>

      <h4>Using a compiled language</h4>

        <p>Using a compiler to run a program is a two-step process:
          <ol>
            <li>Compile the source program to the target language.</li>
            <li>Run the compiled program on a platform for the target language.</li>
          </ol>
        </p>

        <p>For example, in GCC:
          <ol>
            <li>Compile the C program to machine code.</li>
            <li>Run the machine code program on a physical processor.</li>
          </ol>
        </p>

      <h4>Compilers as platforms</h4>

        <p>A compiler is often considered a platform for its source language.</p>

        <p>A compiler platform runs on top of a platform for the target language.</p>

        <p>GCC is a platform for C, running on top of a processor.</p>

      <h4>Compilers on platforms</h4>

        <p>A compiler is also a program written in some language.</p>

        <p>To run the compiler, we need a platform for it.</p>

        <p>GCC is mostly written in C and executes as machine code (usually).</p>

      <h4>The languages of a compiler</h4>

        <p>So a compiler has four related languages:
          <ul>
            <li>Source language: what goes in to the compiler.</li>
            <li>Target language: what comes out of the compiler.</li>
            <li>Implementation language: what the compiler is written with.</li>
            <li>Execution language: what the compiler is run with.</li>
          </ul>
        </p>

        <p>To review, for GCC:
          <ul>
            <li>Source language: C</li>
            <li>Target language: machine code</li>
            <li>Implementation language: C (mostly)</li>
            <li>Execution language: machine code</li>
          </ul>
        </p>

      <p>None of these four languages need to be the same.</p>

    <h3>Bootstrapping</h3>

      <p>Wait, how can GCC be implemented in its own source language?</p>

      <p>No magic, just a clever little trick called <i>bootstrapping</i>.</p>

      <h4>Bootstrapping with another compiler</h4>

        <p>Assume we already have a compiler for some language "X" on our platform.
          <ol>
            <li>Implement a C compiler in X, let's call it "XCC".</li>
            <li>Use the existing X compiler to compile XCC.</li>
            <li>Implement GCC in C.</li>
            <li>Use XCC to compile GCC.</li>
            <li>Use that compiled version of GCC to compile future versions of GCC.</li>
          </ol>
        </p>

      <h4>Bootstrapping with machine code</h4>

        <p>What if there is no suitable "X" on our platform?</p>

        <p>Same process, just with machine code.
          <ol>
            <li>Implement a C compiler in machine code, let's call it "ASMCC".</li>
            <li>Implement GCC in C.</li>
            <li>Use ASMCC to compile GCC.</li>
            <li>Use that compiled version of GCC to compile future versions of GCC.</li>
          </ol>
        </p>

        <p>This is how compilers are created for brand new processor architectures.</p>

    <h3>Interpreters</h3>

      <p>An <i>interpreter</i> is a program that translates code directly into results.</p>

      <h4>Interpreters vs. compilers</h4>

        <p>A compiler reads a <span class="code">print</span> statement and outputs machine code for printing something.</p>

        <p>An interpreter reads a <span class="code">print</span> statement and prints something.</p>

      <h4>Interpreter examples</h4>

        <p>We usually think of software interpreters: CPython, V8, ...</p>

        <p>But a processor is also kind of an "interpreter" of its machine code.</p>

      <h4>Using an interpreted language</h4>

        <p>Using an interpreter to run a program is a one-step process:
          <ol>
            <li>Run the interpreter with the program as input.</li>
          </ol>
        </p>

        <p>For example, in CPython:
          <ol>
            <li>Run CPython with the Python program as input.</li>
          </ol>
        </p>

      <h4>The languages of an interpreter</h4>

        <p>An interpreter has a source language, but no target language.</p>

        <p>For CPython:
          <ul>
            <li>Source language: Python</li>
            <li>Implementation language: C and Python</li>
            <li>Execution language: machine code</li>
          </ul>
        </p>

      <h4>Just-in-time compilation (JIT)</h4>

        <p><i>Just-in-time compilation</i> (JIT) is a core technique for speeding up interpreters.</p>

        <p>Very roughly, a JIT interpreter is as fast as compiled code <i>most</i> of the time.</p>

        <p>The details are beyond the scope of this course.</p>

        <p>(We only have so much time!)</p>

        <p>Dr. Tolmach sometimes teaches a "Modern Language Processors" course that covers it.</p>

    <h3>Virtual machines</h3>

      <p>There are two related but different meanings of "virtual machine".</p>

      <h4>Virtualization</h4>

        <p><i>Virtualization</i> is a way to run one operating system inside another.</p>
        
        <p><a href="https://www.virtualbox.org/">VirtualBox</a> is a popular example.</p>

        <p>These "virtual machines" are studied in the field of operating systems (OS).</p>

        <p>This is not quite the meaning of "virtual machine" that we're concerned with.</p>

      <h4>Emulation</h4>

        <p>Remember: machine code is also a programming language.</p>

        <p>Assume we have two processor architectures, "X" and "Y".</p>

        <p>We can write an interpreter for X's machine code that runs on Y!</p>
        
        <p>We could also write a compiler from X's machine code to Y's machine code.</p>

        <p>This lets us run programs made for X on Y instead.</p>

        <p>This is known as <i>emulation</i>.</p>

      <h4>Emulation example</h4>

        <p>The two dominant processor architectures today are x86 and ARM.</p>

        <p>Apple has been using x86 for years, but recently introduced ARM MacBooks.</p>

        <p><a href="https://en.wikipedia.org/wiki/Rosetta_(software)#Rosetta_2">Rosetta 2</a> is an emulator that allows x86 Mac software to run on ARM MacBooks.</p>

      <h4>Imaginary processors</h4>

        <p>Why stop at emulating real-world processors?</p>

        <p>Real-world machine code is a compromise between PL and digital electronics.</p>

        <p>A <i>virtual machine</i> is a processor architecture designed to be emulated instead of built.</p>

        <p>The machine code of a virtual machine is often called its <i>bytecode</i>.</p>

      <h4>Virtual machine example</h4>

        <p>The Java compiler <span class="code">javac</span> does not compile to machine code.</p>

        <p>Instead, it compiles to Java Virtual Machine (JVM) bytecode.</p>

        <p>The <span class="code">java</span> interpreter then emulates the JVM on a real processor.</p>

      <h4>Portability</h4>

        <p>A compiled Java program is <i>portable</i>: it can run anywhere that has a JVM emulator.</p>

        <p>Well, an x86 machine code program can run anywhere that has an x86 emulator...</p>

        <p>But the JVM is designed for emulation, and x86 very much isn't.</p>

        <p>Also, any new compiler can target JVM bytecode and benefit from its portability.</p>

        <p>This means less work for compiler authors.</p>

      <h4>Optimization</h4>

        <p>Modern JVM emulators are heavily optimized from decades of compiler research.</p>

        <p>Any compiler targeting JVM bytecode can benefit from these optimizations.</p>

        <p>This also means less work for compiler authors.</p>

    <h3>The World Wide Web</h3>

      <p>We'll be working with the TypeScript language in this course.</p>

      <p>TypeScript code runs in a web browser.</p>

      <p>Wait - what exactly does that mean?</p>

      <h4>The Internet</h4>

        <p>Technically, the World Wide Web is not the same thing as the Internet.</p>

        <p>Each term describes a different collection of technologies.</p>

        <p>Very roughly, the Internet is how router hardware works.</p>

        <p>(The Internet is covered in networking courses.)</p>

      <h4>The Web</h4>

        <p>The World Wide Web, or just "the web", is how <i>web browsers</i> work.</p>

        <p>Its precise definition is maintained by the <a href="https://www.w3.org/">World Wide Web Consortium (W3C)</a>.</p>

        <p>We will be taking a very PL-oriented view of the web.</p>

        <p>Web development courses can give you a more rounded view.</p>

        <p>The web includes three notable languages:
          <ul>
            <li>HTML: a <i>markup language</i> for page layout</li>
            <li>CSS: a <i>configuration language</i> for visual styling</li>
            <li>JavaScript (JS): a <i>programming language</i> for interactivity</li>
          </ul>
        </p>

        <p>A web browser is a <i>platform</i> for these languages.</p>

      <h4>Web machine code</h4>

        <p>The web platform is a very weird <i>virtual machine</i>.</p>

        <p>JS is the "machine code" of this virtual machine.</p>

        <p>(There's also WebAssembly, but it can only be run from within JS.)</p>

        <p>A web browser is an <i>emulator</i> for this virtual machine.</p>

        <p>This means a web browser has an <i>interpreter</i> for JS.</p>

      <h4>Compiling to the web</h4>

        <p>For many years, all interactive web code was written in JS.</p>

        <p>(Well, all W3C-compliant code: ignoring Flash, ActiveX, etc.)</p>

        <p>Eventually, other languages started <i>compiling to JS</i>.</p>

        <p>This lets us use non-JS languages to write web code.</p>

      <h4>JS without the web</h4>

        <p>JS can also be run outside of a browser. Node.js is the most popular way.</p>

        <p>Node.js shares much of its code with Chrome: the core JS interpreter code.</p>

        <p>This shared interpreter code is called V8.</p>

        <p>Chrome and Node.js provide different <i>runtime environments</i> for the V8 interpreter.</p>

        <p>Chrome provides ways to interact with the browser in JS code.</p>

        <p>Node.js provides ways to interact with the operating system in JS code.</p>

    <h3>Running TypeScript code</h3>

      <p>Take a moment to breathe...</p>

      <p>How exactly does TypeScript code run?</p>

      <p>We have two interpreters and a compiler.</p>

      <h4>The compiler</h4>

        <p>The TypeScript compiler is called <span class="code">tsc</span>.</p>

        <ul>
          <li>Source language: TypeScript</li>
          <li>Target language: JavaScript (in a browser)</li>
          <li>Implementation language: TypeScript</li>
          <li>Execution language: JavaScript (in Node.js)</li>
        </ul>

      <h4>The compiler's interpreter</h4>

        <p>The TypeScript compiler runs within Node.js, in the V8 interpreter.</p>

        <ul>
          <li>Source language: JavaScript (in Node.js)</li>
          <li>Implementation language: C++</li>
          <li>Execution language: machine code</li>
        </ul>

      <h4>The program's interpreter</h4>

        <p>Our compiled output runs within a browser, in the V8 interpreter or similar.</p>

        <ul>
          <li>Source language: JavaScript (in a browser)</li>
          <li>Implementation language: varies, usually C/C++</li>
          <li>Execution language: machine code</li>
        </ul>

      <h4>Seriously?</h4>

        <p>This can sound a little ridiculous on paper, but there are benefits!</p>

        <p>Remember the idea of <i>portability</i>.</p>

        <p>Many common real-world programs work like this behind the scenes.</p>

        <p>Luckily, as users, we don't have to think very hard about it.</p>

        <p>We'll walk through the setup and use of <span class="code">tsc</span> in lecture.</p>

      <h4>Running TypeScript in practice</h4>

        <p>The output of <span class="code">tsc</span> is a <span class="code">.js</span> file.</p>

        <p>This file gets referenced in a <span class="code">.html</span> file, which is loaded in a web browser.</p>

    <h3>What is TypeScript?</h3>

      <p>This was all about how TypeScript code gets run.</p>

      <p>What kind of language features does TypeScript have?</p>

      <p>How does its feature set compare to other languages?</p>

      <p>We'll explore these questions as we learn more about PL theory!</p>

  </body>
</html>

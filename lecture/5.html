<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 5: CS 358, Fall 2022</title>
  </head>

  <body>
    <h1>CS 358, Fall 2022</h1>
    <h2>Lecture 5: Syntax and lexical analysis</h2>

    <h3>Introduction</h3>

      <p>Last week we explored the <i>phase structure</i> of an interpreter.</p>

      <p>This week we're going to zoom in on the <i>syntax analysis</i> phase.</p>

      <p>Syntax analysis processes <i>text</i> into <i>abstract syntax trees</i>.</p>

      <p>This is a very deep topic that we don't have much time for!</p>

      <p>For a much deeper study, I recommend the <a href="https://www.cs.princeton.edu/~appel/modern/">Modern Compiler Implementation</a> books or other compiler engineering resources.</p>

    <h3>Symbol name review</h3>

      <p>As a software developer, you should know the names of the symbols on a standard keyboard!</p>

      <p>These will all be relevant to us in this course:</p>

      <ul>
        <li><b><span class="code">.   </span></b> "period" or "dot"</li>
        <li><b><span class="code">,   </span></b> "comma"</li>
        <li><b><span class="code">:   </span></b> "colon"</li>
        <li><b><span class="code">;   </span></b> "semicolon"</li>
        <li><b><span class="code">-   </span></b> "dash" or "hyphen"</li>
        <li><b><span class="code">_   </span></b> "underscore"</li>
        <li><b><span class="code">&   </span></b> "ampersand"</li>
        <li><b><span class="code">#   </span></b> "pound" or "hash"</li>
        <li><b><span class="code">@   </span></b> "at sign"</li>
        <li><b><span class="code">%   </span></b> "percent sign"</li>
        <li><b><span class="code">*   </span></b> "asterisk" or "star"</li>
        <li><b><span class="code">+   </span></b> "plus" (or "cross" in British English)</li>
        <li><b><span class="code">/   </span></b> "forward slash" or just "slash"</li>
        <li><b><span class="code">\   </span></b> "backslash"</li>
        <li><b><span class="code">|   </span></b> "pipe" or "bar"</li>
        <li><b><span class="code">'   </span></b> "single quote"</li>
        <li><b><span class="code">"   </span></b> "double quote"</li>
        <li><b><span class="code">`   </span></b> "backtick"</li>
        <li><b><span class="code">~   </span></b> "tilde" or "squiggle"</li>
        <li><b><span class="code">!   </span></b> "exclamation mark" or "bang"</li>
        <li><b><span class="code">?   </span></b> "question mark"</li>
        <li><b><span class="code">^   </span></b> "caret", pronounced like "carrot"</li>
        <li><b><span class="code">$   </span></b> "dollar sign"</li>
        <li><b><span class="code">( ) </span></b> "parenthesis" or just "paren", plural "parentheses"</li>
        <li><b><span class="code">{ } </span></b> "curly bracket" or "curly brace"</li>
        <li><b><span class="code">[ ] </span></b> "square bracket" or "square brace"</li>
        <li><b><span class="code">< > </span></b> "angle bracket" or "less/greater than sign"</li>
      </ul>

      <p>In particular, make sure you know "forward slash" vs. "backslash".</p>

      <p>These names are tied to a left-to-right writing system.</p>

      <p>A forward slash "leans forward": with gravity, it would fall to the right.</p>

      <p>A backslash "leans backward": with gravity, it would fall to the left.</p>

    <h3>Syntax</h3>

      <p>The word <i>syntax</i> refers to the "form" or "shape" or "structure" of a program.</p>

      <p>It's hard to give a very good short definition of the concept.</p>

      <p>(That's why the word <i>syntax</i> exists!)</p>

      <p>It's easiest to define "syntax" through examples and in contrast to things that are not syntax.</p>

      <h4>In natural language</h4>

        <p>The term <i>natural language</i> refers to a language used by humans to talk to other humans.</p>

        <p>Natural languages include English, Spanish, Korean, Arabic, etc.</p>

        <p>The word "syntax" comes to us from linguistics, the study of natural language.</p>

        <p>The <i>syntax rules</i> of a natural language are its <b>spelling and grammar</b>.</p>

      <h4>In programming languages</h4>

        <p>The <i>syntax rules</i> of a programming language are also its "spelling" and "grammar".</p>

        <p>We just mean these words in slightly different ways.</p>

        <p>The "spelling" rules define what is a valid <i>token</i>.</p>

        <p>The "grammar" rules define what is a valid <i>syntax tree</i>.</p>

        <p>We'll define these terms below.</p>

    <h3>Semantics</h3>

      <p>The term <i>semantics</i> is defined in contrast to <i>syntax</i>.</p>

      <p>The <i>semantics</i> of an expression is its <b>actual meaning</b>.</p>

      <p>The word "semantics" is both singular and plural: "a semantics", "some semantics".</p>

      <p>The word "semantic" is an adjective: "semantic properties", "semantic argument".</p>

      <h4>In natural language</h4>

        <p>Consider the English phrase "Portland, Oregon".</p>

        <p>Syntax: "Portland, Oregon" includes 14 letters.</p>

        <p>Syntax: "Portland, Oregon" is a proper noun phrase.</p>

        <p>Syntax: "Portland, Oregon" and "the largest city in Oregon" are different phrases.</p>

        <p>Semantics: Portland, Oregon has a population of 650k people.</p>

        <p>Semantics: Portland, Oregon is a state in the United States.</p>

        <p>Semantics: Portland, Oregon is the largest city in Oregon.</p>

      <h4>In programming languages</h4>

        <p>Consider the C code "1 + 2 * 3".</p>

        <p>Syntax: "1 + 2 * 3" includes three digits.</p>

        <p>Syntax: "1 + 2 * 3" <b>is</b> a numeric <i>expression</i>.</p>

        <p>Syntax: "1 + 2 * 3" is a different expression than "3 * 2 + 1 + 0".</p>

        <p>Semantics: 1 + 2 * 3 evaluates to 7.</p>

        <p>Semantics: 1 + 2 * 3 <b>has</b> a numeric <i>value</i>.</p>

        <p>Semantics: 1 + 2 * 3 has the same value as 3 * 2 + 1 + 0.</p>

    <h3>Syntax analysis</h3>

      <p>During syntax analysis, we are <b>not running the program</b>.</p>
      
      <p>We are also <b>not checking the program for logical errors</b>.</p>

      <p>Make sure to remember this!</p>

      <p>These things are semantics, and the phases <b>after</b> syntax analysis deal with semantics.</p>

      <h4>Goal statement</h4>

        <p>Syntax analysis processes <i>text</i> into an <i>abstract syntax tree</i>.</p>

        <p>As part of this process, it also reports "spelling" and "grammar" errors in the input program.</p>

      <h4>Algorithm structure</h4>

        <p>Our syntax analysis algorithm will have its own <i>phase structure</i>:</p>

        <figure class="phases_figure">
          <table class="arrow_table">
            <tr><td class="arrow_label">text</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">tokenizing</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">tokens</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">parsing</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">AST</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">desugaring</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">AST</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
        </figure>

        <p>These each roughly correspond to features of syntax in natural language:</p>

        <ul>
          <li><i>tokenizing</i>: dividing characters/strokes into words, spell checking</li>
          <li><i>parsing</i>: dividing words into sentences, grammar checking</li>
          <li><i>desugaring</i>: expanding shorthand (abbreviations, contractions, etc.)</li>
        </ul>

        <p>This means that a more detailed view of our whole interpreter could look like this very wide figure:</p>

        <figure class="phases_figure">
          <table class="arrow_table">
            <tr><td class="arrow_label">bits</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">source input</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">text</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">tokenizing</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">tokens</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">parsing</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">AST</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">desugaring</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">AST</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">static analysis</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">AST</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">optimization</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">AST</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
          <span class="phase">execution</span>
          <table class="arrow_table">
            <tr><td class="arrow_label">results</td></tr>
            <tr><td class="arrow">&rightarrow;</td></tr>
          </table>
        </figure>

        <p>These lecture notes will focus on tokenizing.</p>

    <h3>Tokenizing</h3>

      <p>The source input phase gives us one big string of text.</p>

      <p>In <i>tokenizing</i>, we break this string up into chunks to process.</p>

      <p>Tokenizing is also called <i>lexical analysis</i> or <i>lexing</i>.</p>

      <p>In general use, the word <i>lexical</i> means "related to words".</p>

      <p>In natural language, spell-checking is a form of "lexical analysis".</p>

      <p>Grammar checking is <b>not</b> lexical analysis: it falls under <i>parsing</i>.</p>

      <h4>Purpose</h4>

        <p>Consider the C expression "123 + &nbsp;&nbsp;&nbsp;4 * 5".</p>

        <p>There are five meaningful pieces: "123", "+", "4", "*", and "5".</p>

        <p>"123", "4", and "5" are numbers, and "+" and "*" are symbols.</p>

        <p>There are also a bunch of spaces that we don't care about.</p>

        <p><i>Tokenizing</i> is this process of analysis.</p>

      <h4>Tokens</h4>

        <p>The output of tokenizing is a <i>list</i> (or array) of <i>tokens</i>.</p>

        <p>A <i>token</i> is a single indivisible piece of syntax.</p>

        <p>"1 + 2" isn't a token because it can be divided up into "1", "+", and "2".</p>

        <p>"123" is a token because dividing it up any further changes its meaning.</p>

      <h4>Token sorts</h4>

        <p>Each token belongs to a category called a <i>sort</i>.</p>

        <p>(Unrelated to "sorting an array", this is "sort" as a noun.)</p>

        <p>In English, "3" is a different sort than "three".</p>

        <p>Token sorts help the parsing phase know how to process each token.</p>

        <p>It's often convenient to give each different operator symbol its own unique sort.</p>

      <h4>Algorithm</h4>

        <p>A tokenizing algorithm is usually defined with <i>regular expressions</i>.</p>

        <p>We specify each <i>token sort</i> with a regular expression.</p>

        <p>A <i>tokenizer generator</i> program does the rest of the work for us.</p>

    <h3>Regular expressions</h3>

      <p>I recommend <a href="https://regex101.com">regex101.com</a> as a reference.</p>

      <p>Beware: the terminology is really confusing here.</p>

      <h4>Terminology</h4>

        <p>The term "regular expressions" refers to a <i>programming language</i>.</p>

        <p>Actually, multiple different but similar programming languages.</p>

        <p>We'll be using the ECMAScript (JavaScript) version of regular expressions.</p>

        <p>A "regular expression" (or "regex" or "regexp") is a <i>program</i> in the language of regular expressions.</p>

      <h4>Syntax</h4>

        <p>Most commands in the regex language are one or two characters long.</p>

        <p>Regexes pack a lot of information into very little source code.</p>

        <p>A usual 10-character regex is like a 20-line C program, at least.</p>

        <p><b>Work slowly.</b> Don't expect to understand a regex quickly just because it's short.</p>

      <h4>Semantics</h4>

        <p>A regular expression defines a <i>string matching</i> program.</p>

        <p>Think of it as a program that takes in a <span class="code">string</span> and returns a <span class="code">boolean</span>.</p>

        <p>We say that a string <i>matches</i> the regex if it returns <span class="code">true</span>.</p>

        <p>(It's also possible to have a regex return a string, but we won't cover that here.)</p>

        <p>A regex processes a string one character at a time, from left to right.</p>

      <h4>Benefits and limitations</h4>

        <p>Regular expressions are <b>very efficient</b> both in theory and in practice.</p>

        <p>Regular expressions <b>cannot detect missing parentheses</b> or many other complex text patterns.</p>

        <p>See CS 311 for the explanation of why not!</p>

      <h4>Font convention</h4>

        <p>In these notes, regex code will have a light grey background, <span class="regex">like this</span>.</p>

        <p>TypeScript code is still written without a background, <span class="code">like this</span>.

        <p>Make sure to pay attention to the difference between regex code and TypeScript code.</p>

      <h4>In TypeScript</h4>

        <p>In TypeScript, a regex <b>begins and ends with the <span class="code">/</span> character</b>.</p>

        <p>These slashes are <b>not part of the meaning of the regex</b>.</p>

        <p>They're just how it's written in TypeScript code, like quotes around strings.</p>

        <p>If you see <span class="regex">(?:x\d)*</span> in these notes, that's written as <span class="code">/(?:x\d)*/</span> in TypeScript.</p>

        <p>Regular expressions have the type <span class="code">RegExp</span>.</p>

    <h3>Regex commands</h3>

      <p>We will not cover the entire ECMAScript regex language, just the basics.</p>

      <h4>Primitives</h4>

        <p>Each primitive regex command either <i>accepts</i> or <i>rejects</i> the first character of the input string.</p>

        <p>If the input is <i>rejected</i>, the regex returns <span class="code">false</span>, and we say that the match failed.</p>

        <p>If the input is <i>accepted</i>, the program moves to the <b>next</b> character of input, and executes the next command.</p>

        <p>If <b>all</b> characters in the string are accepted, the regex returns <span class="code">true</span>, and we say that the string <i>matches</i> the regex.</p>

        <p>Here are some primitive commands:</p>

        <ul>
          <li><span class="regex">.</span> accepts the current character no matter what it is, or rejects if there are no more characters of input</li>
          <li><span class="regex">\d</span> accepts if the current character is a digit, or rejects otherwise</li>
          <li><span class="regex">\w</span> accepts if the current character is a letter, number, or underscore (<span class="code">'_'</span>), or rejects otherwise</li>
          <li><span class="regex">\s</span> accepts if the current character is a whitespace character (space, tab, or newline), or rejects otherwise</li>
          <li>For any letter or number, or <b>certain</b> other symbols, the character itself is a regex command: for example, <span class="regex">x</span> is a command that accepts the current character if it is <span class="code">'x'</span>, or rejects it otherwise.</li>
        </ul>

      <h4>Sequencing</h4>

        <p>Primitive commands are read left to right. For example:</p>

        <ul>
          <li><span class="regex">xyz</span> matches only the exact string <span class="code">"xyz"</span></li>
          <li><span class="regex">..</span> matches any two characters</li>
          <li><span class="regex">a.</span> matches <span class="code">'a'</span> followed by any character</li>
          <li><span class="regex">p\d.\dq</span> matches <span class="code">'p'</span>, followed by any digit, followed by any character, followed by any digit, followed by <span class="code">'q'</span></li>
        </ul>

      <p><b>Spaces are meaningful:</b>

      <ul>
        <li><span class="regex">\dx</span> matches any digit followed by <span class="code">'x'</span></li>
        <li><span class="regex">\d x</span> matches any digit <b>followed by a space</b> followed by <span class="code">'x'</span></li>
      </ul>

      <h4>Quantifiers</h4>

        <p>We also have some <i>quantifiers</i>, which <b>modify</b> other commands.</p>

        <p>A quantifier is written <b>after</b> the command that it modifies.</p>

        <ul>
          <li><span class="regex">?</span> means "do this command zero or one times"</li>
          <li><span class="regex">*</span> means "do this command zero or more times"</li>
          <li><span class="regex">+</span> means "do this command one or more times"</li>
        </ul>

        <p>In each quantifier, the modified command is executed <b>as many times as possible without rejecting</b>, within the quantifier's bounds.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">x?y</span> matches only <span class="code">"y"</span> and <span class="code">"xy"</span></li>
          <li><span class="regex">x*y</span> matches <span class="code">"y"</span>, <span class="code">"xy"</span>, <span class="code">"xxy"</span>, <span class="code">"xxxy"</span>, etc.</li>
          <li><span class="regex">x*y*</span> matches any number of <span class="code">'x'</span> followed by any number of <span class="code">'y'</span></li>
          <li><span class="regex">x+\d*</span> matches one or more <span class="code">'x'</span> followed by any number of digits</li>
        </ul>

      <h4>Quantifier order of operations</h4>

        <p>Quantifiers come <b>before</b> sequencing.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">xy*</span> matches strings including <span class="code">"xyyy"</span>, <b>not</b> <span class="code">"xyxyxy"</span></li>
          <li><span class="regex">test string*</span> matches strings including <span class="code">"test strin"</span> and <span class="code">"test stringgggg"</span>, <b>not</b> <span class="code">"test string test string"</span> or <span class="code">"test string string"</span></li>
        </ul>

      <h4>Grouping</h4>

        <p>To <i>group</i> commands, we use <i>non-capturing groups</i>.</p>

        <p>These work like parentheses in arithmetic.</p>

        <p>(We won't cover <i>capturing groups</i> in this course, but the difference is important.)</p>

        <p>A non-capturing group <b>begins</b> with <span class="regex">(?:</span> and <b>ends</b> with <span class="regex">)</span>.</p>

        <p>Note that <span class="regex">(?:</span> has a special meaning different from the <span class="regex">?</span> quantifier!</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">(?:xy)*</span> matches strings including <span class="code">""</span> and <span class="code">"xyxyxy"</span>, <b>not</b> including <span class="code">"xyyy"</span></li>
          <li><span class="regex">(?:xy)?(:?ab)*</span> matches strings including <span class="code">""</span>, <span class="code">"xy"</span>, <span class="code">"ab"</span>, <span class="code">"xyab"</span>, <span class="code">"abab"</span>, <span class="code">"xyabab"</span>, <span class="code">"ababab"</span>, <span class="code">"xyababab"</span>, etc.</li>
        </ul>

      <h4>Alternation</h4>

        <p>The alternation operator modifies <b>two</b> regex commands.</p>

        <ul>
          <li><span class="regex">|</span> means "try the left command, if it rejects then try the right command"</li>
        </ul>

        <p>For example:</p>

        <ul>
          <li><span class="regex">a|b</span> matches only <span class="code">"a"</span> and <span class="code">"b"</span></li>
          <li><span class="regex">a|\d</span> matches only <span class="code">"a"</span>, <span class="code">"0"</span>, <span class="code">"1"</span>, <span class="code">"2"</span>, <span class="code">"3"</span>, <span class="code">"4"</span>, <span class="code">"5"</span>, <span class="code">"6"</span>, <span class="code">"7"</span>, <span class="code">"8"</span>, and <span class="code">"9"</span></li>
          <li><span class="regex">.|q</span> matches any one character, just like <span class="regex">.</span></li>
          <li><span class="regex">(?:0|1)*</span> matches any string of binary digits, including <span class="code">""</span></li>
          <li><span class="regex">(?:0|1)+</span> matches any string of binary digits, not including <span class="code">""</span></li>
        </ul>

      <h4>Alternation order of operations</h4>

        <p>Alternation comes <b>after</b> sequencing.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">abc|xyz</span> matches only <span class="code">"abc"</span> and <span class="code">"xyz"</span>, <b>not</b> <span class="code">"abcyz"</span> or <span class="code">"abxyz"</span></li>
          <li><span class="regex">ab|xy*</span> matches <span class="code">"ab"</span>, <span class="code">""</span>, <span class="code">"xy"</span>, <span class="code">"xyy"</span>, <span class="code">"xyyy"</span>, etc., <b>not</b> <span class="code">"abab"</span> or <span class="code">"xyxy"</span> or <span class="code">"abxyy"</span></li>
        </ul>

      <h4>Character classes</h4>

        <p>As a <b>shorthand</b>, we have <i>character classes</i> to match <b>one</b> character in a set of characters.</p>

        <p>A character class is written between square brackets.</p>

        <p>The character class <span class="regex">[abc]</span> has the same meaning as <span class="regex">(?:a|b|c)</span>.</p>

        <ul>
          <li><span class="regex">[wxyz]</span> matches only <span class="code">"w"</span>, <span class="code">"x"</span>, <span class="code">"y"</span>, and <span class="code">"z"</span></li>
          <li><span class="regex">[012]*</span> matches strings including <span class="code">""</span>, <span class="code">"0"</span>, <span class="code">"1"</span>, <span class="code">"2"</span>, <span class="code">"10"</span>, <span class="code">"11"</span>, <span class="code">"210"</span>, and <span class="code">"011201"</span></li>
          <li><span class="regex">[0123456789]</span> is the same as <span class="regex">\d</span></li>
        </ul>

        <p>Within square brackets, <b>most special characters aren't special</b>.</p>

        <ul>
          <li><span class="regex">[a*b]</span> matches only the strings <span class="code">"a"</span>, <span class="code">"*"</span>, and <span class="code">"b"</span>, <b>not</b> <span class="code">"aaab"</span></li>
        </ul>

        <p>A character class can contain a <i>range</i> of characters.</p>

        <p>A range is written with a start and end character separated by a dash.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">[C-F]</span> matches only <span class="code">"C"</span>, <span class="code">"D"</span>, <span class="code">"E"</span>, and <span class="code">"F"</span></li>
          <li><span class="regex">[0-7]*</span> matches any string of base 8 digits, including <span class="code">""</span></li>
          <li><span class="regex">[A-z]</span> matches single-character strings including <span class="code">"X"</span>, <span class="code">"a"</span>, and <b><span class="code">"["</span></b>, because these are Unicode code point ranges</li>
          <li><span class="regex">[a-Z]</span> is invalid, because <span class="code">'a'</span> comes before <span class="code">'Z'</span> in the Unicode character ordering</li>
        </ul>

        <p>Multiple characters and ranges can be combined within the same character class.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">[A-Za-z]</span> matches any single letter, uppercase or lowercase</li>
          <li><span class="regex">[3-5adg-i]</span> matches only <span class="code">"3"</span>, <span class="code">"4"</span>, <span class="code">"5"</span>, <span class="code">"a"</span>, <span class="code">"d"</span>, <span class="code">"g"</span>, <span class="code">"h"</span>, and <span class="code">"i"</span>
        </ul>

        <p>A <i>negated character class</i> is written with <b><span class="regex">[^</span> instead of <span class="regex">[</span></b> to begin the character class.</p>

        <p>A negated character class matches any character <b>except</b> the listed ones.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">[^x]</span> matches any single character except <span class="code">'x'</span></li>
          <li><span class="regex">[^A-Z5]</span> matches any single character except uppercase letters and <span class="code">'5'</span></li>
        </ul>

      <h4>Escaped characters</h4>

        <p>If <span class="regex">.</span> has a special meaning in a regex, how can we write a regex that matches <b>only</b> the string <span class="code">"."</span>?</p>

        <p>We use <span class="regex">\</span> as an <i>escape character</i>.</p>

        <p>This works for any "special" regex symbol, like <span class="regex">*</span>, <span class="regex">+</span>, etc.</p>

        <p>This also works for the <span class="code">\</span> character itself.</p>

        <p>For example:</p>

        <ul>
          <li><span class="regex">\.</span> matches only <span class="code">"."</span></li>
          <li><span class="regex">\\</span> matches only a single backslash character</li>
          <li><span class="regex">\\\\</span> matches only two backslash characters in a row</li>
          <li><span class="regex">\]\(</span> matches only <span class="code">"]("</span></li>
          <li><span class="regex">.\..</span> matches any character, followed by a <span class="code">'.'</span>, followed by any other character</li>
        </ul>

      <h4>Common regexes</h4>

        <p>USA phone numbers: <span class="regex">\(\d\d\d\)-\d\d\d\-\d\d\d\d</span></p>

        <p>Most email addresses: <span class="regex">\w+@\w+\.\w+</span></p>

        <p>Most web addresses: <span class="regex">https:\/\/\w+\.\w+(?:\/[A-Za-z0-9.%?=]+)*</span></p>

        <p>Strings in most languages (simplified): <span class="regex">"(?:[^"]|(?:\\")|(?:\\n))*"</span></p>

    <h3>Tokenizing with regexes</h3>

      <p>Remember: we use regexes to specify <i>token sorts</i>.</p>

      <p>To specify a whole tokenizer, we give a collection of regexes, one per sort.</p>

      <h4>In TypeScript</h4>

        <p>In our TypeScript code, we define tokenizer rules with an <i>object literal</i>.</p>

        <p>We use <a href="https://github.com/no-context/moo">moo.js</a> to generate our tokenizer from our rules.</p>

        <p>For example:</p>

        <pre class="code">
  const rules: Rules = {
    _: / +/,
    int: /\d+/,
    bool: /(?:true)|(?:false)/,
    name: /[A-Za-z_]\w*,
    plus: /\+/,
    minus: /-/,
    parenL: /\(/,
    parenR: /\)/
  }
        </pre>

        <p>(The <span class="regex">+</span>, <span class="regex">(</span>, and <span class="regex">)</span> symbols have special meanings in a regex, so they have to be escaped with <span class="regex">\</span>. The <span class="regex">-</span> symbol does not have a special meaning outside of square brackets, so it is not escaped.)</p>

        <p>The first rule is special: it tells the tokenizer to <b>skip</b> any sequence of one or more spaces. This is indicated by using an underscore as the rule name.</p>

        <p><b>The order of the rules is important:</b> the <span class="code">bool</span> and <span class="code">name</span> rules given above will both match the input string <span class="code">"true"</span>, but the tokenizer will label <span class="code">"true"</span> as a <span class="code">bool</span> and not a <span class="code">name</span> because the <span class="code">bool</span> rule comes first.</p>

        <p>For example, with these rules, the input string <span class="code">"12  + (  x + -3)"</span> will produce this token list:</p>

        <div class="token-array">
          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>int</td><td>"12"</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>plus</td><td>"+"</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>parenL</td><td>"("</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>name</td><td>"x"</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>plus</td><td>"+"</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>minus</td><td>"-"</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>int</td><td>"3"</td></tr>
          </table>

          <table class="token-table">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>parenR</td><td>")"</td></tr>
          </table>
        </div>

    <h3>Now what?</h3>

      <p>We're not done with syntax analysis!</p>

      <h4>What's done?</h4>

        <p>What have we accomplished with tokenizing?</p>

        <ul>
          <li>We got rid of all the whitespace, so our parser doesn't have to think about whitespace at all.</li>
          <li>We categorized each token with a <i>sort</i>, so our parser can immediately tell the difference between (for example) a number and a variable name.</li>
        </ul>

        <p>As a result, our parsing code will be simpler and faster than it would be without a tokenizer.</p>

      <h4>What's left?</h4>

        <p>We don't have an <i>AST</i> yet.</p>

        <p>We've done the PL equivalent of spell-checking.</p>

        <p>Now we have to do grammar checking!</p>

        <p>In the <i>parsing</i> step, we will analyze the <b>structure</b> of the input program.</p>

  </body>

</html>

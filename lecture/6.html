<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../style/lecture.css">
    <title>Lecture 6: CS 358, Fall 2022</title>
  </head>

  <body>
    <h1>CS 358, Fall 2022</h1>
    <h2>Lecture 6: Parsing theory</h2>

    <h3>Introduction</h3>

      <p>In the previous lecture notes, we explored <i>tokenizing</i> with <i>regular expressions</i>.</p>

      <p>As we noted, we don't have an <i>AST</i> yet.</p>

      <p>Remember the internal phase structure of our syntax analysis phase:</p>

      <figure class="phases_figure">
        <table class="arrow_table">
          <tr><td class="arrow_label">text</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">tokenizing</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">tokens</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">parsing</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
        <span class="phase">desugaring</span>
        <table class="arrow_table">
          <tr><td class="arrow_label">AST</td></tr>
          <tr><td class="arrow">&rightarrow;</td></tr>
        </table>
      </figure>

      <p>We have our tokens, so now we need <i>parsing</i>.</p>

      <p>These notes will cover the theory of parsing and introduce <i>context-free grammars</i>.</p>

      <p>The next set of lecture notes will cover parser implementation.</p>

    <h3>Operator syntax</h3>

      <p>The hardest part of parsing is getting operators right.</p>

      <p>We have two main concerns when parsing operators: <i>precedence</i> and <i>associativity</i>.</p>

      <p>Both of these properties affect the <i>order of operations</i> in our language.</p>

      <p>These are best illustrated by example:</p>

      <ul>
        <li>The <i>associativity</i> of the <span class="code">-</span> operator determines whether <span class="code">1 - 2 - 3</span> evaluates to <span class="code">-4</span> or <span class="code">2</span>.</li>
        <li>The <i>precedence</i> of the <span class="code">+</span> and <span class="code">*</span> operators determines whether <span class="code">1 + 2 * 3</span> evaluates to <span class="code">9</span> or <span class="code">7</span>.</li>
      </ul>

      <p>Parentheses <b>override</b> both precedence and associativity.</p>

      <p>These decisions are made <b>before</b> runtime, when we <b>construct</b> the AST.</p>

      <h4>Associativity</h4>

        <p>(This is <b>not the same</b> as the "associative law" from mathematics!)</p>

        <p>The property of <i>associativity</i> is only relevant to <i>infix</i> operators: those that go <b>between two operands</b>, like addition, subtraction, and multiplication.</p>

        <p>Each operator in our language is either <i>left-associative</i> or <i>right-associative</i>.</p>

        <p>Consider a hypothetical infix operator written as <span class="code">@</span>.</p>

        <p>If we write <span class="code">x @ y</span>, we will get this AST:</p>

        <ast-tree>
          <ast-node data-name="@">
            <ast-node data-name="x"></ast-node>
            <ast-node data-name="y"></ast-node>
          </ast-node>
        </ast-tree>

        <p>What AST should we get for <span class="code">x @ y @ z</span>?</p>

        <p>There are two possible choices:</p>

        <div class="ast-array">
          <ast-tree>
            <ast-node data-name="@">
              <ast-node data-name="@">
                <ast-node data-name="x"></ast-node>
                <ast-node data-name="y"></ast-node>
              </ast-node>
              <ast-node data-name="z"></ast-node>
            </ast-node>
          </ast-tree>

          <ast-tree>
            <ast-node data-name="@">
              <ast-node data-name="x"></ast-node>
              <ast-node data-name="@">
                <ast-node data-name="y"></ast-node>
                <ast-node data-name="z"></ast-node>
              </ast-node>
            </ast-node>
          </ast-tree>
        </div>

        <p>In other words, <span class="code">x @ y @ z</span> must produce the same AST as either <span class="code">(x @ y) @ z</span> or <span class="code">x @ (y @ z)</span>.</p>

        <p>If <span class="code">@</span> is <i>left-associative</i>, its trees "lean to the left":</p>

        <ast-tree>
          <ast-node data-name="@">
            <ast-node data-name="@">
              <ast-node data-name="x"></ast-node>
              <ast-node data-name="y"></ast-node>
            </ast-node>
            <ast-node data-name="z"></ast-node>
          </ast-node>
        </ast-tree>

        <p>("...as we all did in the sixties", Roger Hindley)</p>

        <p>If <span class="code">@</span> is <i>right-associative</i>, its trees "lean to the right":</p>

        <ast-tree>
          <ast-node data-name="@">
            <ast-node data-name="x"></ast-node>
            <ast-node data-name="@">
              <ast-node data-name="y"></ast-node>
              <ast-node data-name="z"></ast-node>
            </ast-node>
          </ast-node>
        </ast-tree>

        <p>To build a parser, we must specify <i>left-</i> or <i>right-associativity</i> for each infix operator in our language.</p>

        <p>For <b>some</b> operators, the choice is somewhat arbitrary:</p>

        <ul>
          <li><span class="code">(1 + 2) + 3 = 1 + (2 + 3)</span></li>
          <li><span class="code">(1 - 2) - 3 &ne; 1 - (2 - 3)</span></li>
        </ul>

        <p>But even for the <span class="code">+</span> operator, <span class="code">(1 + 2) + 3</span> and <span class="code">1 + (2 + 3)</span> are different ASTs!</p>

        <p>Even though they both have the same <i>value</i>, they have different <b>computational behavior</b>.</p>

        <p>It's important to be able to <b>predict</b> the computational behavior of a program, not just its final value!</p>

      <h4>Precedence</h4>

        <p>The property of <i>precedence</i> is relevant to <b>all</b> operators in our language, not just infix operators.</p>

        <p>This includes <i>prefix</i> operators like negation, which come <b>before</b> a single operand.</p>

        <p>We traditionally specify precedence as an integer.</p>

        <p><b>Lower precedence</b> means <b>lower priority</b> (evaluates later).</p>

        <p><b>Higher precedence</b> means <b>higher priority</b> (evaluates sooner).</p>

        <p>For example, in a traditional order of operations:</p>

        <ol class="order-of-operations">
          <li>negation</li>
          <li>exponentiation</li>
          <li>multiplication/division</li>
          <li>addition/subtraction</li>
        </ol>

        <ul>
          <li><span class="code">1 * 2 + -3 ^ 4 * 5 = (1 * 2) + (((-3) ^ 4) * 5)</span></li>
        </ul>

        <p>In the reverse order of operations:</p>

        <ol class="order-of-operations">
          <li>addition/subtraction</li>
          <li>multiplication/division</li>
          <li>exponentiation</li>
          <li>negation</li>
        </ol>

        <ul>
          <li><span class="code">1 * 2 + -3 ^ 4 * 5 = 1 * (2 + -((3 ^ 4) * 5))</span></li>
        </ul>

        <p>When two operators have the same precedence, the order of operations is decided by <i>associativity</i>.</p>

        <p>If <span class="code">+</span> and <span class="code">-</span> have the same precedence and are both <i>left-associative</i>:</p>

        <ul>
          <li><span class="code">1 + 2 - 3 = (1 + 2) - 3</span></li>
          <li><span class="code">1 - 2 + 3 = (1 - 2) + 3</span></li>
        </ul>

        <p>If <span class="code">+</span> and <span class="code">-</span> have the same precedence and are both <i>right-associative</i>:</p>

        <ul>
          <li><span class="code">1 + 2 - 3 = 1 + (2 - 3)</span></li>
          <li><span class="code">1 - 2 + 3 = 1 - (2 + 3)</span></li>
        </ul>

        <p>If <span class="code">+</span> and <span class="code">-</span> have the same precedence and each has a <b>different</b> associativity (uncommon):</p>

        <ul>
          <li><span class="code">1 + 2 - 3     </span> is a parsing error</li>
          <li><span class="code">1 - 2 + 3     </span> is a parsing error</li>
        </ul>

    <h3>Grammar</h3>

      <p>Let's step back from code for a second.</p>

      <p>How do we analyze <i>grammar</i> in natural language?</p>

      <p>Every natural language has <i>grammar rules</i>.</p>

      <p>Different languages may have very different rules, but they all have rules.</p>

      <h4>Prescriptive grammar</h4>

        <p>A <i>prescriptive</i> grammar is an <b>authoritative</b> specification of the rules of a language.</p>

        <p>Prescriptive grammar rules say how a language <b>should be</b> used, according to some recognized authority.</p>

        <p>Most natural languages do not have a single widely-accepted prescriptive grammar.</p>

        <p>A few natural languages do have some prescriptive elements. For example:</p>

        <ul>
          <li>The <a href="https://wikipedia.org/wiki/Korean_Language_Society">Korean Language Society</a> created the prescriptive phonetics (pronunciation) for the official Korean Hangul writing system.</li>
          <li>The <a href="https://wikipedia.org/wiki/Acad%C3%A9mie_Fran%C3%A7aise">French Academy</a> maintains France's official prescriptive dictionary of French words (lexical grammar).</li>
          <li>The <a href="https://wikipedia.org/wiki/Turkish_Language_Association">Turkish Language Association</a> maintains Turkey's official prescriptive dictionary of Turkish words (lexical grammar).</li>
        </ul>

        <p>In each case, the prescriptive specification comes from a recognized authority.</p>

      <h4>Descriptive grammar</h4>

        <p>Most natural languages do not have a single widely-accepted prescriptive grammar!</p>

        <p>English is one notable example: there is no widely-recognized authority that publishes a <i>prescriptive</i> specification of English.</p>

        <p><a href="https://www.merriam-webster.com/">Merriam-Webster</a> does not claim to be an <b>authority</b> on English lexical grammar.</p>

        <p>Instead, they document the ways that people are already using English.</p>

        <p>A <i>descriptive grammar</i> describes how a language <b>is currently</b> used, according to someone studying a population who uses the language.</p>

        <p>This is an important difference in the field of linguistics, which is usually more <i>descriptive</i> than <i>prescriptive</i>.</p>

      <h4>Programming language grammar</h4>

        <p>Programming languages have <i>prescriptive grammar</i>.</p>

        <p>This kind of sucks sometimes, but we haven't really taught computers natural language yet.</p>

        <p>The ultimate "authority" on the syntax of the language is the author of the parser.</p>

        <p>The ultimate specification of the syntax of the language is the parser code itself.</p>

        <p>To create a parser, we most often use <i>context-free grammars</i>.</p>

    <h3>Context-free grammars (CFGs)</h3>

      <p>A <i>context-free grammar</i> (or <i>CFG</i>) describes a set of valid <i>parse trees</i> in a language.</p>

      <p>Parse trees are also called <i>concrete syntax trees</i>: closely related to ASTs, but not quite the same.</p>

      <h4>Purpose</h4>

        <p>Like regexes, CFGs can be thought of as a programming language for processing strings.</p>

        <p>Each CFG describes a function that takes in a <span class="code">string</span> and returns a <i>set</i> (or array) of ASTs.</p>

        <p>We generally call these <i>parsing functions</i> or just <i>parsers</i>.</p>

        <p>The parsers that we generate from CFGs can be very efficient.</p>

        <p>This is pretty great! Writing efficient parsers completely by hand is a pain.</p>

      <h4>Tools</h4>

        <p>There are tools for generating parsers from CFGs in nearly every programming langauge.</p>

        <p>These tools are called <i>parser generators</i>.</p>

        <p>An older, very confusing term for them is <i>compiler compilers</i>.</p>

        <p>In our assignments, we will use the <a href="https://nearley.js.org/">Nearley</a> parser generator library.</p>

        <p>(Don't install it manually, that'll be done automatically in the assignment projects.)</p>

        <p>We'll explore Nearley parsers in more depth in the next set of lecture notes.</p>

      <h4>Terminology</h4>

        <p>There are a couple key terms we need to establish first:</p>

        <ul>
          <li>A <i>terminal</i> is just a <i>token sort</i>.</li>
          <li>A <i>nonterminal</i> is a special kind of <i>variable</i> defined by the rules of the CFG</li>
        </ul>

        <p>In Nearley notation:</p>

        <ul>
          <li>We write a terminal with a <span class="code">%</span> prefix, like <span class="code">%float</span> or <span class="code">%name</span> or <span class="code">%plus</span>.</li>
          <li>We write a nonterminal without a prefix, like <span class="code">expression1</span> or <span class="code">atom</span>.</li>
        </ul>

      <h4>Production rules</h4>

        <p>We define a CFG as a set of <i>production rules</i>.</p>

        <p>Each production rule has two components, separated by an arrow:</p>

        <ul>
          <li>On the left, a <b>single</b> nonterminal.</li>
          <li>On the right, a <b>sequence</b> of terminals and nonterminals.</li>
        </ul>

        <p>There may be more than one rule with the same nonterminal on the left.</p>

        <p>Each CFG also has a <i>start symbol</i>, which is a nonterminal.</p>

        <p>Traditionally, the start symbol is the nonterminal on the left of the first (topmost) rule.</p>

        <p>Other than this, the order of the rules is not important, with one minor exception we'll see later.</p>

        <p>Here's the first example CFG we'll consider, in Nearley notation:</p>

        <div class="cfg code">
          <span class="rule-from">expr</span>
          <span class="rule-arrow"/>-&gt;</span>
          <span class="rule-to">expr %plus expr</span>

          <span class="rule-from">expr</span>
          <span class="rule-arrow"/>-&gt;</span>
          <span class="rule-to">%negate expr</span>

          <span class="rule-from">expr</span>
          <span class="rule-arrow"/>-&gt;</span>
          <span class="rule-to">%float</span>
        </div>

        <p>There are three terminals: <span class="code">%plus</span>, <span class="code">%negate</span>, and <span class="code">%float</span>.</p>

        <p>There is only one nonterminal, <span class="code">expr</span> (short for "expression"). It is the start symbol.</p>

      <h4>Syntax tree derivation</h4>

        <p>A CFG can be used to <i>derive</i> (or <i>generate</i>) parse trees.</p>

        <p>We have a little game to play, which is called <i>derivation</i>.</p>

        <ol type="A">
          <li>Start by writing down the start symbol. (Leave lots of space below it, and to the left and right.)</li>
          <li>Pick any rule in the CFG that has the start symbol on the left. If there's more than one, pick any one you want.</li>
          <li>For each terminal and nonterminal on the right side of the rule, create a child in the tree under the start symbol. For each terminal child that you create, choose a string that matches the token sort that the terminal represents.</li>

          <li>Repeat steps A-C for each nonterminal leaf in the tree until there are only token leaves in the tree. If there's more than one nonterminal leaf, do them in any order you want.</li>
        </ol>

        <p><div>In parse trees, we often use notation like <span class="code">%float(1)</span> to represent the token
          <table class="token-inline">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>float</td><td>"1"</td></tr>
          </table>.
        </div></p>

        <p><div>When there is only a single string in a token sort, we often leave out the string and only write the sort: for example, in a parse tree, <span class="code">%plus</span> represents the token
          <table class="token-inline">
            <tr><th>sort</th><th>text</th></tr>
            <tr><td>plus</td><td>"+"</td></tr>
          </table>.
        </div></p>

        <p>For example, using our example grammar from above:</p>

        <ol class="derivation">
          <li>
            <div class="cfg code">
              <span class="rule-from"></span>
              <span class="rule-arrow"/>(start)</span>
              <span class="rule-to"></span>
            </div>
            <ast-tree>
              <ast-node data-name="expr"></ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">expr %plus expr</span>
            </div>

            <ast-tree>
              <ast-node data-name="expr" data-color="green">
                <ast-node data-name="expr"></ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>

            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr" data-color="green">
                  <ast-node data-name="%float(1)"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%negate expr</span>
            </div>

            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr">
                  <ast-node data-name="%float(1)"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr" data-color="green">
                  <ast-node data-name="%negate"></ast-node>
                  <ast-node data-name="expr"></ast-node>
                </ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>

            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr">
                  <ast-node data-name="%float(1)"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr">
                  <ast-node data-name="%negate"></ast-node>
                  <ast-node data-name="expr" data-color="green">
                    <ast-node data-name="%float(2)"></ast-node>
                  </ast-node>
                </ast-node>
              </ast-node>
            </ast-tree>
          </li>
        </ol>

        <p>Okay, we followed the steps and there are no nonterminal leaves left.</p>

        <p>What have we accomplished?</p>

        <p>Read the terminal strings left to right: <span class="code">1 + - 2</span>.</p>

        <p>We have just <i>derived</i> this expression using our CFG.</p>

        <p>This means it is a valid expression in our language.</p>

        <p>In general, if there is <b>any way</b> to derive an expression using a CFG, it is a valid expression in the language.</p>

        <p>If there is <b>no possible way</b> to derive an expression using a CFG, it is not a valid expression in the language.</p>

      <h4>Parsing errors</h4>

        <p>If there is <b>no possible way</b> to derive an expression using a CFG, we say the expression has a <i>parsing error</i>.</p>

        <p>For example, using our example grammar from above again: let's try to derive <span class="code">1 + + 2</span>, which should be invalid.</p>

        <ol class="derivation">
          <li>
            <div class="cfg code">
              <span class="rule-from"></span>
              <span class="rule-arrow"/>(start)</span>
              <span class="rule-to"></span>
            </div>
            <ast-tree>
              <ast-node data-name="expr"></ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">expr %plus expr</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr" data-color="green">
                <ast-node data-name="expr"></ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr" data-color="green">
                  <ast-node data-name="%float(1)"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr">
                  <ast-node data-name="%float(1)"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr" data-color="green">
                  <ast-node data-name="%float(2)"></ast-node>
                </ast-node>
              </ast-node>
            </ast-tree>
          </li>
        </ol>

        <p>Well, that didn't work: we got <span class="code">1 + 2</span>.</p>

        <p>That's not enough, though: maybe different choices would have worked?</p>

        <p>We had to get a <span class="code">"+"</span> in there, so there were no other choices in step 2.</p>

        <p>We could have made a different choice in step 3: let's try to get a second <span class="code">"+"</span> in there.</p>

        <ol class="derivation">
          <li>
            <div class="cfg code">
              <span class="rule-from"></span>
              <span class="rule-arrow"/>(start)</span>
              <span class="rule-to"></span>
            </div>
            <ast-tree>
              <ast-node data-name="expr"></ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">expr %plus expr</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr" data-color="green">
                <ast-node data-name="expr"></ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">expr %plus expr</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr" data-color="green">
                  <ast-node data-name="expr"></ast-node>
                  <ast-node data-name="%plus"></ast-node>
                  <ast-node data-name="expr"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr">
                  <ast-node data-name="expr" data-color="green">
                    <ast-node data-name="%float(1)"></ast-node>
                  </ast-node>
                  <ast-node data-name="%plus"></ast-node>
                  <ast-node data-name="expr"></ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr">
                  <ast-node data-name="expr">
                    <ast-node data-name="%float(1)"></ast-node>
                  </ast-node>
                  <ast-node data-name="%plus"></ast-node>
                  <ast-node data-name="expr" data-color="green">
                    <ast-node data-name="%float(1)"></ast-node>
                  </ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr"></ast-node>
              </ast-node>
            </ast-tree>
          </li>

          <li>
            <div class="cfg code">
              <span class="rule-from">expr</span>
              <span class="rule-arrow"/>-&gt;</span>
              <span class="rule-to">%float</span>
            </div>
            <ast-tree>
              <ast-node data-name="expr">
                <ast-node data-name="expr">
                  <ast-node data-name="expr">
                    <ast-node data-name="%float(1)"></ast-node>
                  </ast-node>
                  <ast-node data-name="%plus"></ast-node>
                  <ast-node data-name="expr">
                    <ast-node data-name="%float(1)"></ast-node>
                  </ast-node>
                </ast-node>
                <ast-node data-name="%plus"></ast-node>
                <ast-node data-name="expr" data-color="green">
                  <ast-node data-name="%float(2)"></ast-node>
                </ast-node>
              </ast-node>
            </ast-tree>
          </li>
        </ol>

        <p>Nope: we got <span class="code">1 + 1 + 2</span>.</p>

        <p>Step 3 went wrong again. Could we have made any other choice?</p>

        <p>Only the one that would have introduced a <span class="code">"-"</span>, which is also wrong.</p>

        <p>What if we started with the right <span class="code">expr</span> instead of the left <span class="code">expr</span>?</p>

        <p>We'd just have the mirror image of the same problem.</p>

        <p>There is <b>no possible move</b> to choose in step 3 that will lead us to derive <span class="code">1 + + 2</span>.</p>

        <p>This means <span class="code">1 + + 2</span> is not in our language, and has a parsing error.</p>

    <h3>Looking ahead</h3>

      <p>Most <i>parsers</i> are built on this theory of derivations.</p>

      <p>We haven't seen yet how to specify <i>precedence</i> and <i>associativity</i> in a CFG.</p>

      <p>In the next lecture notes, we'll explore how to craft CFGs to make good parsers.</p>

      <p>You should practice a few derivations on paper!</p>

      <p>Doing derivations by hand will help with debugging CFGs when things go wrong.</p>

      <p>(We don't have great interactive debuggers for CFGs, unfortunately.)</p>

  </body>

</html>

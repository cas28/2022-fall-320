class DerivationTreeElement extends HTMLElement {
  constructor() {
    super();

    const shadowRoot = this.attachShadow({ mode: "open" });

    const style = shadowRoot.appendChild(document.createElement("style"));
    style.innerHTML = `
      table {
        border-collapse: collapse;
      }

      .container {
        padding: 0 0.75em;
        display: inline-flex;
        flex-wrap: nowrap;
        justify-content: space-around;
        align-items: flex-end;
      }

      .ruleCell {
        vertical-align: bottom;
        transform: translateY(-0.75em);
      }

      .conclusionCell {
        border-top: 1px solid black;
      }
    `;

    const template = document.createElement("template");
    template.innerHTML = `
      <div class="container">
        <div class="ruleCell"><slot name="rule"></slot></div>
        <div class="derivCell">
          <table>
            <tr><td><slot name="premise"></slot></td></tr>
            <tr><td align="center" class="conclusionCell"><slot name="conclusion"></td></tr>
          </table>
        </div>
      </div>
    `;

    shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("deriv-tree", DerivationTreeElement);

customElements.define("deriv-node", class extends DerivationTreeElement {
  constructor() { super(); }
});